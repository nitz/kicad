
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x10, 0x08, 0x04, 0x00, 0x00, 0x00, 0xb5, 0xfa, 0x37,
 0xea, 0x00, 0x00, 0x01, 0x5a, 0x49, 0x44, 0x41, 0x54, 0x28, 0xcf, 0x85, 0xd1, 0x4f, 0x28, 0x83,
 0x71, 0x1c, 0xc7, 0xf1, 0xe7, 0xa0, 0x28, 0xae, 0xae, 0x94, 0xdc, 0x14, 0xed, 0xf9, 0x3d, 0xb3,
 0xc7, 0xd6, 0xe6, 0x41, 0x48, 0x44, 0x22, 0x7f, 0xa6, 0xb9, 0xcc, 0x86, 0x93, 0x49, 0x9a, 0x5c,
 0xd4, 0x24, 0xd2, 0x53, 0x0e, 0x76, 0x79, 0x92, 0x93, 0x22, 0x17, 0x07, 0x39, 0x70, 0x99, 0x8b,
 0x2c, 0x27, 0x07, 0x2b, 0xb5, 0x9b, 0x13, 0x49, 0xf6, 0xf8, 0x0d, 0x31, 0xab, 0xb7, 0x0b, 0xd3,
 0x94, 0x7c, 0xbf, 0xb7, 0x6f, 0xaf, 0xfa, 0x7e, 0xfb, 0x7e, 0x14, 0xe5, 0xbf, 0x72, 0x54, 0x8b,
 0x07, 0x71, 0x2b, 0x92, 0xea, 0x9e, 0x66, 0x69, 0xeb, 0x62, 0x4d, 0xac, 0x69, 0x71, 0x75, 0x5b,
 0x3b, 0x14, 0x97, 0xe2, 0x5e, 0x3c, 0x3a, 0x6b, 0x14, 0x47, 0xb5, 0xb0, 0x05, 0x7f, 0x74, 0xc6,
 0x59, 0xa3, 0x28, 0x8a, 0xa8, 0xf2, 0xbe, 0x9e, 0x73, 0xc0, 0x38, 0x73, 0x58, 0x58, 0x4c, 0x11,
 0xe1, 0x98, 0x33, 0x5c, 0x39, 0xa3, 0xe4, 0x6b, 0x8d, 0xfe, 0x72, 0x83, 0xc4, 0xc4, 0x42, 0x22,
 0x59, 0x62, 0x17, 0x49, 0x12, 0x23, 0x5d, 0xb8, 0xc3, 0xb8, 0xf2, 0x11, 0xc0, 0xcf, 0x10, 0x26,
 0x26, 0x3d, 0x04, 0x19, 0xa3, 0x8d, 0xe6, 0xbd, 0x02, 0xf0, 0x6c, 0xc5, 0x49, 0x12, 0xc3, 0x8f,
 0x85, 0x45, 0x37, 0x9b, 0x5c, 0xb0, 0xf8, 0xa6, 0x46, 0x0a, 0x40, 0x0d, 0x47, 0x9f, 0x25, 0xa7,
 0x84, 0x91, 0x48, 0xba, 0x49, 0x23, 0x19, 0xcd, 0xa8, 0xad, 0x3f, 0x40, 0xef, 0x78, 0xe9, 0x25,
 0x84, 0x81, 0x89, 0x49, 0x23, 0x33, 0x0c, 0xe0, 0xca, 0x3b, 0x2a, 0x0b, 0xa0, 0xbe, 0x5c, 0xcf,
 0xa5, 0xd8, 0xc7, 0x8b, 0x45, 0x9c, 0x26, 0x8e, 0x48, 0xe0, 0xc9, 0x14, 0xbd, 0xcb, 0x77, 0x97,
 0xc2, 0x46, 0x27, 0xc3, 0x35, 0xbd, 0x48, 0x4e, 0x68, 0x49, 0x16, 0x01, 0x23, 0xd1, 0x4a, 0x18,
 0x37, 0xcb, 0x44, 0x69, 0x67, 0x82, 0x4e, 0x3c, 0x1b, 0x45, 0xc0, 0x15, 0x5b, 0xc9, 0x27, 0xe8,
 0x62, 0x95, 0x05, 0x46, 0x38, 0x65, 0x3a, 0xab, 0x05, 0x8a, 0x80, 0x36, 0x10, 0xb6, 0x25, 0x93,
 0x24, 0xd8, 0x21, 0x86, 0xa4, 0xeb, 0xc9, 0xd1, 0xf0, 0x2b, 0xb2, 0xc6, 0xf7, 0x41, 0xbb, 0x2f,
 0x37, 0xfb, 0x11, 0xca, 0x0f, 0xbf, 0xf7, 0xdb, 0x7a, 0xb6, 0xb6, 0xf4, 0x57, 0xaa, 0x75, 0x15,
 0xaa, 0x57, 0x0b, 0xaa, 0xf3, 0xea, 0xbc, 0x16, 0x74, 0xba, 0x8d, 0xb2, 0xef, 0xf9, 0x27, 0xd6,
 0xc2, 0xda, 0xdb, 0x3d, 0x2e, 0xe6, 0x4c, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae,
 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE trash_xpm[1] = {{ png, sizeof( png ), "trash_xpm" }};

//EOF
