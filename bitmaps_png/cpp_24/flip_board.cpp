
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x01, 0x68, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0x60, 0xc0, 0x07, 0x1a,
 0xfe, 0x33, 0x49, 0x35, 0x1e, 0x79, 0x07, 0xc4, 0x47, 0xa4, 0x1a, 0x0e, 0x4f, 0x94, 0x6e, 0x3a,
 0x1c, 0x27, 0xd1, 0x74, 0x44, 0x1b, 0x24, 0xce, 0x40, 0x15, 0xb0, 0x6a, 0x15, 0x33, 0xd0, 0xf0,
 0xff, 0x58, 0xf0, 0x27, 0x90, 0xa5, 0xd2, 0x0d, 0x47, 0x67, 0x4a, 0x36, 0x1e, 0xce, 0x97, 0x6a,
 0x38, 0x62, 0xa3, 0xd0, 0xb0, 0x9f, 0x83, 0xa0, 0x79, 0xbb, 0x76, 0xed, 0x2a, 0x02, 0xe2, 0x0e,
 0x28, 0xce, 0x87, 0xfa, 0xe0, 0x3f, 0x31, 0x58, 0xa6, 0xe9, 0xc8, 0x1f, 0xdd, 0x8e, 0xc3, 0xcf,
 0x6d, 0x7a, 0x0f, 0x1d, 0xc1, 0x69, 0xc1, 0xce, 0x9d, 0x3b, 0xff, 0xed, 0xd8, 0xb1, 0xa3, 0x1c,
 0x84, 0x81, 0xec, 0x34, 0x3c, 0x3e, 0xc0, 0x8b, 0xf1, 0x5a, 0x40, 0x64, 0x10, 0x0d, 0x5e, 0x0b,
 0x22, 0x68, 0x6a, 0x01, 0x3a, 0x08, 0x8c, 0x89, 0xd1, 0xa7, 0x99, 0x05, 0x21, 0x21, 0x21, 0x4d,
 0x40, 0xfc, 0x92, 0xaa, 0x16, 0x00, 0x53, 0x4f, 0x14, 0x88, 0x0e, 0x0d, 0x0d, 0x4d, 0x00, 0x1a,
 0x7e, 0xd7, 0x3b, 0x2a, 0x4a, 0x89, 0x26, 0x91, 0x0c, 0x34, 0xfc, 0x06, 0xd0, 0x12, 0x3f, 0x9a,
 0xa4, 0x22, 0xa0, 0xc1, 0x3c, 0x40, 0x0b, 0xfe, 0x00, 0x69, 0x4e, 0x9a, 0x58, 0xe0, 0xe0, 0xe0,
 0xc0, 0x02, 0xb4, 0xe0, 0x7b, 0x42, 0x42, 0x02, 0x07, 0xcd, 0xf2, 0x01, 0xd0, 0x82, 0xa3, 0x34,
 0x09, 0x22, 0x58, 0x24, 0x07, 0x07, 0x07, 0x7b, 0x01, 0x2d, 0xb9, 0xed, 0x97, 0x92, 0x22, 0x0e,
 0xd2, 0x20, 0x5f, 0xb7, 0xe7, 0xbf, 0x1c, 0x10, 0x53, 0x3b, 0x99, 0xf6, 0x01, 0xf1, 0x73, 0x93,
 0x9c, 0x89, 0xff, 0x2d, 0x32, 0x3b, 0xff, 0x03, 0xd9, 0xff, 0x7d, 0x22, 0x12, 0xff, 0x3b, 0x26,
 0x57, 0xfe, 0x37, 0xcb, 0xee, 0xfb, 0xaf, 0x51, 0xb6, 0x82, 0xf2, 0x8c, 0x16, 0x10, 0x12, 0xe2,
 0xe9, 0x90, 0x5c, 0xf5, 0xdf, 0x27, 0x2a, 0x19, 0x6c, 0x01, 0x08, 0x7b, 0x45, 0xa7, 0xfd, 0x37,
 0xcd, 0x99, 0x00, 0xf6, 0x15, 0xd5, 0x8a, 0x0a, 0xf5, 0xb2, 0x55, 0xff, 0xad, 0xd3, 0x9b, 0xff,
 0xab, 0x56, 0xac, 0x1b, 0x2d, 0x4d, 0x69, 0x5c, 0xe1, 0x20, 0x57, 0x99, 0x40, 0x4b, 0x0a, 0x48,
 0xb1, 0x00, 0xd8, 0x20, 0xf8, 0xa3, 0xd3, 0x7e, 0xf8, 0x85, 0x55, 0xcf, 0xc1, 0x63, 0x03, 0x57,
 0xe9, 0xe3, 0xb0, 0xe0, 0x99, 0x74, 0xe3, 0x91, 0x6d, 0xd2, 0x0d, 0x47, 0x5a, 0xa5, 0x1b, 0x0f,
 0x87, 0xca, 0x34, 0x1c, 0x53, 0x61, 0xf8, 0xff, 0x9f, 0x91, 0x58, 0x23, 0x00, 0x56, 0x0c, 0x2f,
 0x42, 0x86, 0xe0, 0x8b, 0x2d, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60,
 0x82,
};

const BITMAP_OPAQUE flip_board_xpm[1] = {{ png, sizeof( png ), "flip_board_xpm" }};

//EOF
