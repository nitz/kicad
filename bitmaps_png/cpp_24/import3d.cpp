
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x8a, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xed, 0x94, 0x4b, 0x6c, 0x12,
 0x51, 0x14, 0x86, 0xc7, 0xc4, 0xa8, 0x89, 0xba, 0x70, 0xe3, 0xc2, 0xb8, 0xd6, 0x58, 0xac, 0x8a,
 0x2c, 0x34, 0xb0, 0xd2, 0xfa, 0xa2, 0x34, 0xda, 0x04, 0x58, 0x40, 0x42, 0x62, 0xa2, 0x2d, 0xac,
 0x5d, 0x98, 0x2e, 0x4c, 0xb0, 0x76, 0xc1, 0xa6, 0x1a, 0x34, 0x35, 0x12, 0x6b, 0x4d, 0x50, 0xbb,
 0x10, 0x43, 0x14, 0x6d, 0x74, 0x48, 0xa8, 0x33, 0x0c, 0x98, 0x98, 0x22, 0xb1, 0xc6, 0xc6, 0xd6,
 0x07, 0x50, 0x6d, 0x8c, 0x14, 0x79, 0xa4, 0x3c, 0xaa, 0x28, 0x70, 0x3c, 0x77, 0x64, 0x90, 0x11,
 0x1a, 0x1e, 0xeb, 0x92, 0x7c, 0x61, 0x66, 0xee, 0xb9, 0xff, 0x3f, 0x73, 0xce, 0x3d, 0x87, 0x52,
 0xab, 0xd5, 0x52, 0x8d, 0x46, 0x53, 0x42, 0xa0, 0x15, 0xb4, 0x5a, 0xad, 0x8a, 0x6a, 0xe6, 0x87,
 0xc1, 0x8f, 0x5a, 0x15, 0x2f, 0xf3, 0x0a, 0xb7, 0xaf, 0x69, 0x68, 0x20, 0x93, 0xc9, 0x52, 0x48,
 0xa1, 0x1d, 0x24, 0x12, 0xc9, 0xa6, 0x66, 0x0c, 0x32, 0x08, 0xb4, 0x83, 0x42, 0xa1, 0xd8, 0xbc,
 0x6a, 0xb0, 0x6a, 0xd0, 0x9e, 0x81, 0x4e, 0xa7, 0x03, 0x87, 0xc3, 0x01, 0xc5, 0x62, 0xf1, 0x3d,
 0x00, 0x2c, 0x23, 0x59, 0x64, 0x06, 0xb1, 0x22, 0x92, 0xb6, 0x0d, 0xe4, 0x72, 0x39, 0x38, 0x9d,
 0x4e, 0xf8, 0x11, 0x8d, 0x41, 0xe8, 0xf2, 0x28, 0x4c, 0xf5, 0x9c, 0x01, 0xef, 0x9e, 0x13, 0xc0,
 0xed, 0x55, 0x42, 0xa0, 0xb7, 0x1f, 0xc2, 0xd6, 0xdb, 0xf0, 0x2b, 0x91, 0x2a, 0xa0, 0xc9, 0x08,
 0xb2, 0xae, 0x25, 0x03, 0x22, 0x1e, 0x0c, 0x06, 0x61, 0xf1, 0xc9, 0x24, 0x70, 0x52, 0x15, 0x30,
 0x3b, 0x0e, 0xd5, 0xc5, 0xb7, 0x5f, 0x05, 0x31, 0xda, 0x8b, 0xfa, 0x30, 0xc9, 0x9b, 0x18, 0x8d,
 0xc6, 0x5b, 0x26, 0x93, 0x09, 0x1a, 0xc1, 0x71, 0x1c, 0x44, 0x1f, 0x7b, 0x80, 0xd9, 0x79, 0x58,
 0x24, 0x38, 0xdd, 0x37, 0x00, 0xef, 0x2e, 0x0c, 0x8b, 0x8d, 0x30, 0x26, 0x46, 0xb3, 0xc4, 0x64,
 0x84, 0xc2, 0xcd, 0x5b, 0x0d, 0x06, 0xc3, 0x73, 0xbd, 0x5e, 0xcf, 0xae, 0x84, 0xd9, 0x6c, 0x0e,
 0xfc, 0x5c, 0xfc, 0x0e, 0xdc, 0xbe, 0xee, 0x9a, 0x37, 0x4e, 0x7e, 0x8a, 0x40, 0x2e, 0x97, 0x03,
 0xdf, 0x81, 0x5e, 0xd1, 0x73, 0xf2, 0x95, 0x24, 0x5d, 0x64, 0xd8, 0xd9, 0x1a, 0x0d, 0x36, 0x9a,
 0xa6, 0x21, 0x34, 0x7c, 0xb3, 0x6e, 0x4a, 0x52, 0xf3, 0x0b, 0xbc, 0xc1, 0x0b, 0x85, 0xba, 0x66,
 0x2d, 0x6c, 0x1d, 0x83, 0xa6, 0x86, 0x5d, 0x24, 0x12, 0x81, 0xa9, 0xee, 0xd3, 0x2d, 0x1b, 0x04,
 0x4e, 0xf5, 0x41, 0x53, 0x45, 0xce, 0xe7, 0xf3, 0xfc, 0x69, 0x11, 0xa5, 0xa0, 0x9c, 0xae, 0x6a,
 0x03, 0x6f, 0xe7, 0x71, 0x60, 0x77, 0x1d, 0x11, 0xc5, 0x50, 0x52, 0xa9, 0xb4, 0x03, 0x45, 0x3a,
 0x09, 0x76, 0xbb, 0x3d, 0xab, 0x54, 0x2a, 0x0f, 0x0a, 0xf7, 0x02, 0xa5, 0x52, 0x29, 0xef, 0xaf,
 0xca, 0xf1, 0x87, 0x2b, 0xa3, 0x90, 0x4d, 0x67, 0x60, 0xe6, 0xbc, 0xa5, 0x62, 0x30, 0xdd, 0x3f,
 0x00, 0x69, 0xac, 0xd3, 0x57, 0xb7, 0xb7, 0x12, 0x47, 0x5e, 0x4a, 0xd4, 0x13, 0x6e, 0xb7, 0x3b,
 0xed, 0x72, 0xb9, 0x6a, 0xba, 0x13, 0x4f, 0xc3, 0x2c, 0x39, 0xe7, 0xc2, 0xc6, 0xb7, 0xe7, 0x86,
 0x78, 0xd1, 0x6c, 0x26, 0x0b, 0x4b, 0xd8, 0x13, 0xe4, 0x3a, 0x13, 0x4f, 0xf0, 0xff, 0xe1, 0xb1,
 0xfb, 0xff, 0x52, 0x74, 0xf2, 0x2c, 0x50, 0xdb, 0x06, 0xfd, 0x05, 0x81, 0xed, 0x83, 0x7e, 0xa8,
 0xbe, 0x17, 0xb0, 0x07, 0xbe, 0x95, 0x3e, 0xdb, 0xc6, 0x45, 0x29, 0x9a, 0x1b, 0xba, 0xc6, 0x0b,
 0x56, 0xf3, 0xe5, 0xc1, 0x04, 0xa6, 0xa8, 0xab, 0x12, 0x33, 0x7f, 0xe3, 0x1e, 0x6f, 0x00, 0x8d,
 0xe8, 0xb2, 0xbd, 0x86, 0xe5, 0x68, 0xbc, 0xa6, 0xc1, 0x66, 0x2f, 0x5d, 0xad, 0x88, 0x2f, 0x3c,
 0xa4, 0x45, 0xe2, 0x3e, 0x59, 0x0f, 0xfc, 0x5e, 0xca, 0x14, 0x9b, 0x32, 0x20, 0xe0, 0x57, 0xd4,
 0x6d, 0xb4, 0x39, 0xcb, 0x75, 0x88, 0xdc, 0x75, 0x8a, 0xc4, 0x49, 0xa1, 0xe3, 0xec, 0xcb, 0xbf,
 0x8d, 0x46, 0x5d, 0x64, 0xd6, 0x0a, 0x3c, 0xa5, 0xdd, 0x69, 0xcb, 0xf8, 0xc4, 0x96, 0xea, 0x67,
 0x02, 0x77, 0xde, 0x44, 0x37, 0x26, 0x92, 0xc9, 0x42, 0xa3, 0x51, 0x41, 0xd6, 0x62, 0xcf, 0xf8,
 0x2e, 0xf6, 0x20, 0xeb, 0xff, 0x2f, 0xf2, 0x47, 0x86, 0x61, 0x36, 0xac, 0x34, 0x79, 0x3d, 0x1e,
 0x4f, 0x08, 0x27, 0xa8, 0x2d, 0x1f, 0x8b, 0x17, 0x49, 0x4d, 0xc8, 0x39, 0xf7, 0xee, 0x3e, 0x06,
 0x6c, 0xc7, 0x51, 0x7e, 0xf0, 0x91, 0x01, 0x48, 0xd6, 0xca, 0xc3, 0x8e, 0xd7, 0xf9, 0x03, 0xfd,
 0x4d, 0x11, 0x24, 0x2b, 0x65, 0xf6, 0x24, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae,
 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE import3d_xpm[1] = {{ png, sizeof( png ), "import3d_xpm" }};

//EOF
